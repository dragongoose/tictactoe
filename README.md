## TicTacToe
A TicTacToe game created in java. Had to meet the following guidelines (for school)
- Two classes
- Readable to new programmers
- use array, function, and inter-class communicaton

## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies
- `bin`: where the project is built

## Running
1. Build: 
`cd src; javac TicTacToe.java -d ../bin; cd ..`

2. Run:
`cd bin; java TicTacToe`

Or oneliner:
`cd src; javac TicTacToe.java -d ../bin; cd ../bin; java TicTacToe`

## License
This project is licensed under the GPL v3 license