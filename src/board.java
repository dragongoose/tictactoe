public class board {
    public static String[] board = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

    public static void checkIfWon() {
        // Uses a clever way to detect if the play has won
        // it will concatenate every column (horizontal),
        // row (vertical), and diagnal (left-right, right-left).
        // if this concatenation is equal to XXX or OOO, we know
        // that player has won.


        for (int i = 1; i < 4; i++) {
            // i could be 1, 2, or 3. columns begin on 0, 3, and 6.
            // multiplying i by 3 and subtracting by 2 will give us
            // the column beginnings as needed, 0, 3, and 6 from the
            // values 1, 2, and 3 respectively
            int col = (i * 3) - 3; // cols are index 0,3,6

            // concat all values in the column
            String boardCol = board[col] + board[col + 1] + board[col + 2];
            if (boardCol.equals("XXX") || boardCol.equals("OOO")) {
                TicTacToe.displayWin();
            }
        }

        // check rows
        for (int i = 0; i < 2; i++) {
             // concat all values in the row
            String boardRow = board[i] + board[i + 3] + board[i + 6];
            if (boardRow.equals("XXX") || boardRow.equals("OOO")) {
                TicTacToe.displayWin();
            }
        }

        // check diag
        // there are only two possibilities with diagnals,
        // so it isn't worth writing it's own loop for,
        // hence the hardcoded locations
        String diag1 = board[0] + board[4] + board[8];
        String diag2 = board[2] + board[4] + board[6];

        if (diag1.equals("XXX") || diag1.equals("OOO")) {
            TicTacToe.displayWin();
        }

        if (diag2.equals("XXX") || diag2.equals("OOO")) {
            TicTacToe.displayWin();
        }
    }

    public static void printBoard() {
        System.out.println("BOARD: \n");
        System.out.println(" " + board[0] + " | " + board[1] + " | " + board[2]);
        System.out.println("---|---|---");
        System.out.println(" " + board[3] + " | " + board[4] + " | " + board[5]);
        System.out.println("---|---|---");
        System.out.println(" " + board[6] + " | " + board[7] + " | " + board[8]);
        System.out.println("\n");
    }
}
