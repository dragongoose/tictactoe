import java.util.Scanner;

public class TicTacToe {
    public static String[] playerMarkers = {"X", "O"};
    public static int currentPlayer = 0;

    public static Scanner keyboard = new Scanner(System.in);
    public static boolean playing = true;

    public static void displayWin() {
        board.printBoard();
        System.out.println("good job player " + (currentPlayer + 1) + "!! you won!");

        // this will only run when won, tence ending the game
        playing = false;
    }

    public static boolean isValidInput(int choice) {
        if (choice < 1 || choice > 10) {
            System.out.println("you can't go here! that spot doesn't exist silly goose :p");
            return false;
        }


        // Gets the default spot of the board and compares
        // it to it's current value. If they do not equal 
        // we know the spot is occupied
        String spotOnBoard = board.board[choice - 1];
        String boardDefaultSpot = Integer.toString(choice);
        if (!spotOnBoard.equals(boardDefaultSpot)) {
            System.out.println("you can't choose this spot silly goose! please choose an unoccupied space >:(");
            return false;
        }

        return true;
    }

    public static void getUserInput() {
        System.out.println("Player " + (currentPlayer + 1) + ", your turn!");
        System.out.println("Where would you like to play?");
        int place = keyboard.nextInt();

        if(!isValidInput(place)) {
            return;
        }

        // this will only run if the move is valid
        board.board[place - 1] = playerMarkers[currentPlayer];
    }

    public static void main(String[] args) throws Exception {
        System.out.println(board.board[8]);

        int i = 0;
        while (playing) {
            currentPlayer = i % 2;

            board.printBoard();
            getUserInput();
            board.checkIfWon();
            i++;
        }
    }
}
